# export baserow_api_key=$(cat ~/.secrets/baserow_api_key) && python3 download.py;git status && git add *.json && git commit -m "downloaded" && git push origin main
# export baserow_api_key=$(cat ~/.secrets/baserow_api_key) && ipython3  

from os import environ
from requests import get
from json import loads, dump
from datetime import datetime

baserow_api_key = environ['baserow_api_key']

headers = {
    "Authorization": "Token " + baserow_api_key
}

tables_data = {
        "marches hebdomadaires": {
            "url": "https://api.baserow.io/api/database/rows/table/47228/?user_field_names=true",
            "filter": "&include=Nom de l\'événement,Lien,format,Régularité et jour,Organisé par (orga),Lieu tenu par orga,ville (déduit via orga),Adresse du lieu,Latitude géo,Longitude géo" + \
                    "&filter_type=OR&filter__field_370429__multiple_select_has=97499&filter__field_370429__multiple_select_has=121608&filter__field_370429__multiple_select_has=138495",
            "output_file": "marches-hebdomadaires.json"
        },
        "entreprises": {
            "url": "https://api.baserow.io/api/database/rows/table/46944/?user_field_names=true",
            "filter": "&include=Dénomination,Site web,Ville,Adresse,Statut membre\/prospect normalisé,Activité (normalisée),NOMS \- fournisseur de (a pour clients),se fournit chez (client de) (via proxy),Noms des structures dont est membre,📆 Événements \- Buvette\/food-truck,Latitude géo,Longitude géo,Marché hebdo le plus proche,Accepte moyens de paiement,Contact dans l\'entreprise,ClusterS\-chaîneS d\'appro,Marché à proximité de la ville" +\
                    "&filter_type=AND&filter__field_279452__not_empty&filter__field_264794__not_empty&filter__field_362665__not_empty",
            "output_file": "entreprises.json"
        },
        
        "activités": {
            "url": "https://api.baserow.io/api/database/rows/table/61966/?user_field_names=true",
            "filter": "&include=Name,type",
            "output_file": "activites.json"
        },
        "contacts": {
            "url": "https://api.baserow.io/api/database/rows/table/46948/?user_field_names=true",
            "filter": "&include=rôle,annuaire etps (prospects et membres)" +\
                    #/!\ données privées, ne pas télécharger de champs qui ne sont pas des anonymisés
            "&filter__field_264847__not_empty",
            "output_file": "contacts.json"
        },
        "presences": {
            "url": "https://api.baserow.io/api/database/rows/table/46137/?user_field_names=true",
            "filter": "&include=" + \
                    ",".join([
                        "Titre",
                        "Date fixée",
                        "description de la mission",
                        "Événement concerné",
                        "Ville de l\'événement (lieu où organisé)"
                        ]) + \
                    "&filter_type=AND" + \
                    "&filter__field_360089__link_row_has=60",
            "output_file": "presences.json"
        }
,
        "evenements": {
            "url": "https://api.baserow.io/api/database/rows/table/47228/?user_field_names=true",
            "filter": "&include=Nom de l'événement,Lien,Date,Organisé par (orga),Lieu tenu par orga,Buvette/food-truck,format,ville (déduit via orga),Régularité et jour,Latitude géo,Longitude géo,Adresse du lieu,Bureau de change permanent le plus proche" +\
                    "&filter__field_266538__not_empty" + \
                    "&filter__field_266590__not_empty" +\
                    "&filter__field_266591__not_empty" +\
                    "&filter__field_266591__date_after=" + datetime.now().strftime("%Y-%m-%d"),
            "output_file": "evenements.json"
        }

}


tables = list(tables_data.keys())

def get_baserow_data(baserow_url):
    return loads(get(
            baserow_url,
            headers=headers
        ).content)

data = {}

for table in tables:
    # Télécharger les données présentes dans baserow
    # Pour toutes les tables : limiter les champs demandés
    # Pour les entreprises : ajouter un filtre pour réduire le nombre
    this_table_baserow_url = tables_data[table]["url"] + tables_data[table]["filter"]
    data[table] = get_baserow_data(this_table_baserow_url)
    next = data[table]["next"]
    while next != None:
        following_data = get_baserow_data(next)
        data[table]["results"] += following_data["results"]

        next = following_data["next"]
    
    dump(data[table]["results"], open(tables_data[table]["output_file"], 'w', encoding='utf8'), ensure_ascii=False)
