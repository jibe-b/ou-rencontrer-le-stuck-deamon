from os import environ
from requests import get, patch
from json import loads
from time import sleep

baserow_api_key = environ['baserow_api_key']

headers_get = {
    "Authorization": "Token " + baserow_api_key,
}

headers_post = headers_get
headers_post["Content-Type"] = "application/json"

# Get cities without GPS in baserow

baserow_urls = {
        "get": "https://api.baserow.io/api/database/rows/table/49423/?user_field_names=true" + "&filter_type=AND" + "&filter__field_414389__empty&filter__field_414390__empty",
        "patch":"https://api.baserow.io/api/database/rows/table/49423/"
}

def get_baserow_data(url):
    return loads(get(url, headers=headers_get).content)

cities = get_baserow_data(baserow_urls["get"])
next = cities["next"]
while next != None:
        following_data = get_baserow_data(next)
        cities["results"] += following_data["results"]
        next = following_data["next"]

print(len(cities["results"]))

for city in cities["results"]:
    name = city["Ville"]
    row_id = city["id"]
    nominatim_url = "https://nominatim.openstreetmap.org/search?q=" + name + ",Bas-Rhin&state=France&limit=1&format=json"

    sleep(1)

    try:
        nominatim_data = loads(get(nominatim_url).content)[0]
        city["Latitude geo"] = nominatim_data["lat"]
        city["Longitude geo"] = nominatim_data["lon"]
    
        for key in ['[D]annuaire entreprises (prospects & membres) et assos',
            '[C]événements',
            "[C] Contacts actifs de l'asso",
            'Adhésions'
            ]:
            city[key] = [item["id"] for item in city[key]]


        response = patch(
            baserow_urls["patch"] + str(row_id) + "/?user_field_names=true",
            headers=headers_post,
            json=city
            ).content
    except:pass

